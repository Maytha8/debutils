#!/usr/bin/env python3
# When developing, run this script inside 'hatch env' to start the CLI.

import sys

from debutils.cli import main

sys.exit(main())
